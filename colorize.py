from skimage import io, color
from skimage import io, color
from skimage import img_as_ubyte

import numpy as np
import random
import math

from tensorflow import keras
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Dropout, Flatten
from tensorflow.keras.layers import Conv2D, MaxPooling2D, Conv2DTranspose, UpSampling2D, BatchNormalization
from tensorflow.keras.models import load_model
from tensorflow.keras.callbacks import ModelCheckpoint

import sys
from os import path
import pathlib

files_trn = [line.strip() for line in open("textures/flowers_trn.txt", 'r')]
files_tst = [line.strip() for line in open("textures/flowers_tst.txt", 'r')]

files_trn = random.sample(files_trn, 400)
files_tst = random.sample(files_tst, 20)

SIZE = None
TAIN_COUNT = len(files_trn)
TEST_COUNT = len(files_tst)
EPOCHS = 200
SIZE_FACTOR = 2**4

CHECKPOINT_PATH = 'checkpoints/flower.ckpt'

print("train count: {}, test count: {}".format(TAIN_COUNT, TEST_COUNT))

def fit_to_size(img):
    w = img.shape[0]
    h = img.shape[1]

    w = math.floor(w/SIZE_FACTOR) * SIZE_FACTOR
    h = math.floor(h/SIZE_FACTOR) * SIZE_FACTOR

    return img[:w,:h]

def read(files):
    im_x = []
    im_y = []
    im_z = []
    for f in files:
        print("reading {}".format(f))
        lab = np.load("textures/np_lab/{}.npy".format(f))

        lab = fit_to_size(lab)

        im_x.append(lab[:,:,0:1])
        im_y.append(lab[:,:,1:3])
        im_z.append(lab)

    return (im_x, im_y, im_z)


trn_X, trn_Y, trn_Z = read(files_trn)
tst_X, tst_Y, tst_Z = read(files_tst)
input_shape = (SIZE, SIZE, 1)
trn_X, trn_Y, trn_Z = np.array(trn_X), np.array(trn_Y), np.array(trn_Z)
tst_X, tst_Y, tst_Z = np.array(tst_X), np.array(tst_Y), np.array(tst_Z)

print("Training images: {}".format(trn_X.shape[0]))
print("Test images:     {}".format(tst_X.shape[0]))



model = Sequential()
batch_size = 10

print(input_shape)

strides = (2,2)

model.add(Conv2D(      16, kernel_size=(3, 3), strides=strides, padding='same', activation='relu', input_shape=input_shape))
model.add(BatchNormalization())
model.add(Conv2D(      32, kernel_size=(3, 3), strides=strides, padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(Conv2D(      32, kernel_size=(3, 3), strides=strides, padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(Conv2D(      64, kernel_size=(3, 3), strides=strides, padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(Conv2D(      64, kernel_size=(5, 5), padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(Conv2D(      64, kernel_size=(5, 5), padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(Conv2D(      64, kernel_size=(5, 5), padding='same', activation='relu'))
model.add(UpSampling2D(size=strides))
model.add(Conv2D(      64, kernel_size=(3, 3), padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(UpSampling2D(size=strides))
model.add(Conv2D(      64, kernel_size=(3, 3), padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(UpSampling2D(size=strides))
model.add(Conv2D(      32, kernel_size=(3, 3), padding='same', activation='relu'))
model.add(BatchNormalization())
model.add(UpSampling2D(size=strides))
model.add(Conv2D(       2, kernel_size=(3, 3), padding='same', activation=None))

model.summary()
#sys.exit()

model.compile(loss='mean_squared_error', optimizer="adam")


if path.exists(CHECKPOINT_PATH + ".index"):
    print("loading model " + CHECKPOINT_PATH)
    model.load_weights(CHECKPOINT_PATH)

cp_callback = ModelCheckpoint(filepath=CHECKPOINT_PATH,
                                save_weights_only=True,
                                verbose=1)
# Subclass ModelCheckpoint
class MyModelCheckpoint(ModelCheckpoint):

    def __init__(self, *args, **kwargs):
        super(MyModelCheckpoint, self).__init__(*args, **kwargs)


    def on_epoch_end(self, epoch, logs=None):
        #if epoch % 10 == 0:
        print("epoch = {}, render_test_set".format(epoch))
        render_test_set(epoch)

cp_render = MyModelCheckpoint(filepath='___.h5')


def render_test_set(epoch):
    pred_Y = model.predict(tst_X)
    #print(pred_Y.shape)

    imgshape = (pred_Y.shape[1], pred_Y.shape[2], 3)


    path = "output/epoch-{}".format(epoch)
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)

    for i in range(pred_Y.shape[0]):
        ab_pred = pred_Y[i,:,:,:] 
        lab_true = tst_Z[i,:,:,:] 
        l_true  = tst_X[i,:,:,:] 

        lab_pred = np.zeros(imgshape)
        lab_pred[:,:,0:1] = l_true
        lab_pred[:,:,1:3] = ab_pred

        
        lab = np.zeros(imgshape)
        lab[:,:,0:1] = l_true
        rgb_gray = color.lab2rgb(lab)
        rgb_pred = color.lab2rgb(lab_pred)
        rgb_true = color.lab2rgb(lab_true)
        

        size0 = imgshape[0]
        rgb_both = np.zeros((size0*3, imgshape[1], 3))
        rgb_both[size0*0:size0*1,:,:] = rgb_true
        rgb_both[size0*1:size0*2,:,:] = rgb_gray
        rgb_both[size0*2:size0*3,:,:] = rgb_pred
        io.imsave("{}/{}".format(path, files_tst[i]), img_as_ubyte(rgb_both))

model.fit(trn_X, trn_Y,
            batch_size=batch_size,
            epochs=EPOCHS,
            callbacks=[cp_callback, cp_render],
            verbose=1)