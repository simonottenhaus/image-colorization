# Image Colorization

Convolutional Neural Networks (CNN) are a fascinating thing.
On one episode of [Two Minute Papers](https://www.youtube.com/watch?v=MfaTOXxA8dM) covers colorization of grayscale image.
Here a CNN is tasked with guessing the colors of a provided back and white image, based on previously seen training data.

I had to try this for myself. So I looked for a simple CNN approach to implement myself and found Colorful Image Colorization by [Zhang et. al](https://richzhang.github.io/colorization/)
I implemented a fully convolutional neural network (FCNN) with a similar architecture.

The first experiment was to colorize pink shoes and an IKEA bag, as shown in the following image:
- The top row contains the ground-truth image
- The middle row contains the grayscale image
- The bottom row contains the colorized image

![alt text](examples/shoes-and-bag.jpg)


## Network
The FCNN is comprised of three parts
- The encoder: reduce the image size
- The latent processing: processing
- The decoder: increase the image size back to the original size

Throughout the network the ReLU function is used as activation function.

### Encoder
The encoder is built with four blocks, where each block contains three parts, namely:
- A convolutional layer with a 3x3 receptive field
- A down sampling layer that halves the image size (implemented as stride in the conv. layer)
- A batch normalization layer

Each layer has 32 filters as output. Through the four blocks the image size is halved four times resulting in an effective resolution reduction by factor 16.

### Latent Processing
The latent processing is performed by three blocks, where each block contains:
- A convolutional layer with a 5x5 receptive field
- A batch normalization layer

### Decoder
The decoder part of the network is implemented as four blocks, where each block contains:
- Up sampling the image by factor 2 (each pixel is copied to a 2x2 block)
- A convolutional layer with a 3x3 receptive field
- A batch normalization layer

### Output
The last layer of the decoder does not contain batch normalization nor an activation function.

## Training and Inference
The images are encoded using the LAB color space, as suggested in the paper by [Zhang et. al](https://richzhang.github.io/colorization/).
The LAB color space encodes the luminance in the L-channel and the color information in the A and B channels.
For training the LAB channels of the images are split.
The L-channel is provided as input to the network.
The AB-channels are provided as output.

During inference only the L-channel is used and provided as input. The FCNN predicts the AB-channels.
For display the original L-channel is combined with the predicted AB-channels, resulting in a LAB-image that can be converted back to RGB.

## Data Acquisition and Pre-processing
To gather enough training data, I took a video of the target scene and converted that video to single images using ffmpeg:
`ffmpeg -i myvideo.mp4 img%04d.jpg`

The images were split into training and test set using all images with numbers xxx0.jpg as test images and all other images for training.
This results in a test set that is quite similar to the training set, but this is fine for this project.
I am not trying to outperform other approaches that were trained on millions of images - I rather want to explore the FCNN image colorization concept here.

## Results
In my opinion the results of the first experiment (shoes and bag) are promising.
As a second experiment I took more training images outside of flowers and the walkway.
The FCNN was retrained to this new dataset. The following picture displays the result. (Top row: original image, middle row: grayscale, bottom row: colorized image)
![alt text](examples/flowers_2rows.jpg)
In some of the grayscale image I find it difficult to find the flowers, however the FCNN does not seem to have any problems.

In a third experiment I verify that the FCNN is only able to colorize what it has seen before.
Here the FCNN is trained on the flower dataset and tested on the shoes-and-bag dataset.
As expected, the results are *interesting*, the network struggles to make sense of the input:
![alt text](examples/cross-test.jpg)

## Conclusion and Future Work
This project shows (again) the power and properties of CNNs:
- They perform well when tested on images, that are similar to the training set
- The performance drops rapidly when tested on images far away from the training data

In future work I want to combine the uncertainty estimation with this image colorization.
This should result in a pixel wise prediction of uncertainty.
E.g. when the FCNN is tested on images it has not been trained for the uncertainty prediction should be large throughout the image.
If only parts of the image are unknown, e.g. a unknown object is introduced, the uncertainty should only be high in this new unknown region.
