from skimage import io, color
import numpy as np
import pathlib

files_trn = [line.strip() for line in open("textures/flowers_trn.txt", 'r')]
files_tst = [line.strip() for line in open("textures/flowers_tst.txt", 'r')]

files = []
files.extend(files_trn)
files.extend(files_tst)

pathlib.Path("textures/np_lab").mkdir(parents=True, exist_ok=True)

for f in files:
    print("reading {}".format(f))
    rgb = io.imread("textures/color/{}".format(f))
    lab = color.rgb2lab(rgb)
    np.save("textures/np_lab/{}.npy".format(f), lab)

